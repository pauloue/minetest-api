% Minetest API Documentation

Minetest has a rich modding API that makes it easy to add content and
functionality through mods and games.

[Mods](mods/)
-------------

Mods and games are written using a Lua modding API.

[Client Mods](client-mods/)
---------------------------

A planned feature for Minetest is server sent client side mods. A client modding
API has been added, but client side mods (CSMs) are currently only client
provided and not server sent.

[Texture Packs](texture-packs/)
-------------------------------

Texture packs can be used to change the default textures of a mod or game.

[Minetest Game API](minetest-game/)
-----------------------------------

Minetest Game is the default game shipped with Minetest. You can write mods that
extend and interact with the game using the game API.
